
clc;
clearvars -except ensemble boundary cities;

%%
addpath('../functions');
addpath('../aus-map/matlab');
addpath('../MCTS2');
addpath('../package-temporal-logic');

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SETUP
%% ENSEMBLE DATASET PATH
base_dataset = fullfile('../../Dataset/bom_ensemble_mat/tasman_16.mat');

%% ENVIRONMENT TRIMING
ax = [-1.450, 10.576, -03.110, 8.904]*1e5; % ENVIRONMENT SIZE = [XMIN, XMAX, YMIN, YMAX] IN METRES AROUND SYDNEY

%% INITIAL AND GOAL POSITIONS
x0 = [2, 0]*1e5;        % STARTING POSITION
x_goal = [5, 7]*1e5;  % GOAL POSITION

rg = 0.1*1e5;   % GOAL RADIUS

%% VEHICLE DYNAMICS
v0 = 0.35;      % vehicle speed
dt = 600*10;    % Duration for each step within action
T = 3600*10;    % Duration for each action
numTh = 20;     % Number of angle actions

%% MCTS SETUP
hrz = 250;  % Number of actions in sequence
numSim = 1; % number of simulation/rollouts
Cp = 0.1;   % Exploration parameter

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
if false
    %%
    clear all;
end

if ~(exist('ensemble', 'var') && exist('boundary', 'var') && exist('cities', 'var'))
    

    %% REFERENCE COORDINATE
    ref = struct();
    ref.lat = -33.8700;
    ref.lon = 151.21;

    %% LOADING
    ensemble = load(base_dataset);
    boundary = load_aus_boundary();
    cities = load_aus_cities();

    %% LAT/LON TO XY
    [ensemble.mX, ensemble.mY] = latlon2xy(ensemble.mLat, ensemble.mLon, ref.lat, ref.lon);
    [boundary.X, boundary.Y] = latlon2xy(boundary.lat, boundary.lon, ref.lat, ref.lon);
    [cities.x, cities.y] = latlon2xy(cities.lat, cities.lon, ref.lat, ref.lon);
    
    ensemble.mPos = [ensemble.mX, ensemble.mY];
end

%% LOAD ENSEMBLE
ens = Ensemble(ensemble.mX, ensemble.mY, ensemble.mU, ensemble.mV, 'usegpu', false);
ens = ens.Trim(ax);

ensavg = ens.TrimAvg();






%% PARAMETER SETUP
Th = rotspace(numTh);
maxA = numel(Th);   % number of angle controls

param = struct();
param.Th = Th;
param.ens = ens;

param.v0 = v0;
param.dt = dt;
param.T = T;
param.x_goal = x_goal;

param.numSim = numSim;
param.hrz = hrz;

P = [x_goal, rg*5];
phi = @(Rp) F(Rp{1});
% phi = @(Rp) F(Rp{1}) & G(~Rp{2});

r0 = norm(x0-x_goal);
% f_score = @(X) min(reward_pt(X, pos_g, r0, rg*5));
f_score = @(X) mean(reward_pt(X, x_goal, r0, rg*5));
% f_score = @(X) min(reward_stl(X, phi, P));

%% DRAW MAP
getfigure(1, true); clf; setFigTight; hold on; axis equal; 
axis(ax);

for ii=1:ens.numE
    quiver(ens.Xe, ens.Ye, ens.Ue(:, ii), ens.Ve(:, ii), 'color', 0.8*ones(1, 3));
end
quiver(ensavg.Xe, ensavg.Ye, ensavg.Ue, ensavg.Ve, 'color', 'b');


plot(boundary.X, boundary.Y, 'black-');
plot(cities.x, cities.y, 'r*');
text(cities.x, cities.y, cities.name);

[cx, cy] = polycircle(50);
for ii=1:size(P, 1)
    fill(P(ii, 1) + cx*P(ii, 3), P(ii, 2) + cy*P(ii, 3), 'g', 'FaceAlpha', 0.5);
end

drawnow;

%% MCTS SETUP
printmsg('MCTS setup');

rng(0);

maxNumCompThreads(20);

f_actions = @(id, node, mcts) ifelse(node.depth<hrz, Th, []);
% f_rollout = @(id, node, mcts) randsteer(atan2m(node.data.X(randi(ens.numE), :, end), pos_g), Th, hrz - node.depth);
f_rollout = @(id, node, mcts) rollout_steer(node, param, 1);
% f_reward = @(aseq_tree, aseq_rand, data_tree) mcts_f_reward(aseq_tree, aseq_rand, data_tree, param, executeEnsembleWrapper, scoreWrapper);
f_reward = @(aseq_tree, aseq_rand, data_tree) f_score(executeEnsemble(x0, [aseq_tree, aseq_rand], ens, dt, T, v0));

f_expand_data = @(a, data_parent) evaluateAction(a, data_parent, param);


mcts = MCTS2(...
    'Cp', Cp, 'data', struct('status', [], 'X', []), 'numSim', numSim, 'parsim', true, ...
    'f_actions', f_actions, 'f_reward', f_reward, 'f_rollout', f_rollout, 'f_expand_data', f_expand_data);
mcts = mcts.init(struct('status', zeros(ens.numE, 1), 'X', repmat(x0, ens.numE, 1)));

score_max = -inf;
aseq_max = [];

getfigure(2, true); clf; hold on; setFigTight
iter = 0;

printmsg('MCTS iteration');
profile off;
profile on;

tic_mcts = tic;

%% MCTS ITERATIONS

% for iter=1:100
while true
    %%
    iter = iter + 1;
    mcts = mcts.grow();
    
    % ADDITIONAL RANDOM ROLLOUT
    aseq_max = mcts.aseq_best{randi(numel(mcts.aseq_best))};
    for ii=1:numSim
        aseq_rand = randsteer(aseq_max, Th, 1);
        id_back = aseq2suffix(aseq_rand, mcts);
        
        score_rand = f_score(executeEnsemble(x0, aseq_rand, ens, dt, T, v0));
        mcts = mcts.backpropagate(id_back, score_rand, {aseq_rand});
    end
    
    getfigure(2);
    try
        delete(h2);
    end
    h2 = mcts.plot();
    title(sprintf('Iter = %d', iter));
    drawnow;
    
    if mcts.F_best > score_max
        score_max = mcts.F_best;
        aseq_max = mcts.aseq_best{1};
        
        [X, Xc] = executeEnsemble(x0, aseq_max, ens, dt, T, v0);
        [Xavg, Xcavg] = executeEnsemble(x0, aseq_max, ensavg, dt, T, v0);
        
        fprintf('[%d: %.1fs] %f\n', iter, toc(tic_mcts), score_max);
        
        getfigure(1);
        title(sprintf('Score = %f', score_max));
        try
            delete(h1);
        end
        h1 = [];
        for eidx=1:size(X, 1)
            h1(end+1) = plot(squeeze(X(eidx, 1, :)), squeeze(X(eidx, 2, :)), 'black-');
        end
        h1(end+1) = plot(squeeze(X(:, 1, end)), squeeze(X(:, 2, end)), 'blackx', 'MarkerSize', 10);
        h1(end+1) = plot(squeeze(Xavg(1, 1, :)), squeeze(Xavg(1, 2, :)), 'r-', 'LineWidth', 2);
        h1(end+1) = plot(squeeze(Xavg(1, 1, end)), squeeze(Xavg(1, 2, end)), 'r.', 'MarkerSize', 10);
        
        pts = vertcatc(cellfun(@(X)X(:, :, 1), Xcavg, 'UniformOutput', false));
        h1(end+1) = quiver_angle(pts(:, 1), pts(:, 2), aseq_max', 0.5, 'magenta-', 'lineWidth', 1);
        
        [~, mindice] = min(squeeze(sqrt((X(:, 1, :)-x_goal(1)).^2 + (X(:, 2, :)-x_goal(2)).^2)), [], 2);
        Xm = nan(ens.numE, 2);
        for eidx=1:ens.numE
            Xm(eidx, :) = X(eidx, :, mindice(eidx));
        end
        
        cindice = convhull(Xm(:, 1), Xm(:, 2), 'simplify', true);
        h1(end+1) = fill(Xm(cindice, 1), Xm(cindice, 2), 'b', 'facealpha', 0.2, 'edgecolor', 'red');
%         cindice = convhull(X(:, 1, end), X(:, 2, end), 'simplify', true);
%         h1(end+1) = fill(X(cindice, 1, end), X(cindice, 2, end), 'b', 'facealpha', 0.5, 'edgecolor', 'none');
        
        
        drawnow;
    end
end

