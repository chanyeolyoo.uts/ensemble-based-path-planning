
function [idx, value] = randbool(array)

% RANDBOOL Randomly selects the non-zero index
% idx = RANDBOOL(array) 

indice = find(array);
idx = indice(randi(numel(indice)));
value = array(idx);