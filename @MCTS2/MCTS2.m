
classdef MCTS2 < Tree3
%MCTS2 Monte Carlo tree search algorithm
%   MCTS2('numSim', 100, ...) specifies the number of simulations per expansion (default: 100))
%   MCTS2('numBest', 1, ...) specifies the number of best simulations (default: 1)
%   MCTS2('Cp', 1, ...) specifies the exploration term for UCB (default: 1)
%
%   Required parameters as follows:
%       MCTS2('f_actions', @(id, node, mcts) ..., ...) 
%       MCTS2('f_rollout', @(id, node, mcts) ..., ...) rolls out from ID to the
%       end (not the full path to ID)
%       MCTS2('f_reward', @(aseq_tree, aseq_rand, node) ..., ...)
%       MCTS2('data', struct(...), ...)
%
%Written by Chanyeol Yoo, Ph.D., University of Technology Sydney, November 2019.

    properties (SetAccess = private)
        Cp;
        numSim;
        numBest;
        parsim;
    end
    
    properties (SetAccess = private)
        f_actions;
        f_rollout;
        f_reward;
        f_expand_data;
    end
    
    properties (SetAccess = private)
        F_best = -inf;
        aseq_best = {};
        
%         best = repmat(struct('F', nan, 'aseq', {}), 1, 0);
        best = struct('F', [], 'aseq', {});
    end
    
    %%
    methods

        function self = MCTS2(varargin)
            %%
            p = inputParser;
            
            p.addOptional('numSim', 100, @isnumeric);
            p.addOptional('numBest', 1, @isnumeric);
            p.addOptional('parsim', false, @islogical);
            p.addOptional('f_actions', [], @(x)isa(x, 'function_handle'));
            p.addOptional('f_rollout', [], @(x)isa(x, 'function_handle'));
            p.addOptional('f_reward', [], @(x)isa(x, 'function_handle'));
            p.addOptional('f_expand_data', [], @(x)isa(x, 'function_handle'));
            p.addOptional('Cp', 1);
            p.addOptional('data', struct(), @(x)isa(x, 'struct'));
            
            p.parse(varargin{:});
            
            %% actionset field parsing
            actionset = struct('A', [], 'arrayA', []);
%             ff = fieldnames(p.Results.data);
%             if any(ismember(fieldnames(data), ff))
%                 error('Additional data fields should not include ''A'' and ''arrayA''');
%             end
%             for ii = 1:length(ff)
%                 data.(ff{ii}) = p.Results.data.(ff{ii});
%             end
            data_st = p.Results.data;
                        
            %%
            self = self@Tree3('N', 0, 'mean', 0, 'ucb', 0, 'rollout', struct('F', -inf, 'aseq', []), 'actionset', actionset, 'data', data_st);
            
            %% OPTIONAL VARIABLES
            self.numSim = p.Results.numSim;
            self.numBest = p.Results.numBest;
            self.parsim = p.Results.parsim;
            self.Cp = p.Results.Cp;
            
            self.f_reward = p.Results.f_reward;
            self.f_actions = p.Results.f_actions;
            self.f_rollout = p.Results.f_rollout;
            self.f_expand_data = p.Results.f_expand_data;
            
            if ~isa(self.f_actions, 'function_handle')
                self.f_actions = @self.getactions;
            end
            if ~isa(self.f_rollout, 'function_handle')
                self.f_rollout = @self.rollout;
            end
            
            %% CHECK FOR PARALLEL COMPUTATION
            if self.parsim == true && isempty(gcp('nocreate')) && self.numSim > 1
                parpool;
            end
        end
        
        %%
        function self = init(self, data)
            self = add(self, 0, []);
            if exist('data', 'var')
                self.G.Nodes(1, :).data = data;
            end
            for aidx=find(self.G.Nodes.actionset.arrayA)
%                 [self, id_added] = self.add(1, aidx);
                [self, id_added] = expand(self, 1, aidx);
%                 [self, Fs] = self.simulate(id_added);
                [self, Fs_sim, aseqs_sim] = self.simulate(id_added);
%                 self = self.backpropagate(id_added, Fs);
                self = self.backpropagate(id_added, Fs_sim, aseqs_sim);
            end
        end
        
        %%
%         function 
        
        %%
        function self = grow(self)
            id_select = self.select();
            [self, id_added] = self.expand(id_select);
%             [self, Fs] = self.simulate(id_expand);
            [self, Fs_sim, aseqs_sim] = self.simulate(id_added);
%             self = self.backpropagate(id_expand, Fs);
            self = self.backpropagate(id_added, Fs_sim, aseqs_sim);
        end
        
        %%
        function id_select = select(self)
            %%
            id = 1;
            while true
                ids_child = find(self.G.Nodes.pid==id);
                if isempty(ids_child)
                    break;
                end

                [~, midx] = randmax(self.G.Nodes.ucb(ids_child));
                id = ids_child(midx);
                if ~all(self.G.Nodes.actionset(id).arrayA==0)
                    break;
                end
            end
            id_select = id;
        end
        
        %%
        function [self, id_expand] = expand(self, id, aidx)
            if isempty(self.G.Nodes.actionset(id).arrayA)
                id_expand = id;
            else
                if ~exist('aidx', 'var')
                    aidx = randbool(self.G.Nodes.actionset(id).arrayA);
                end
                [self, id_expand] = self.add(id, aidx);
                
                if ~isempty(self.f_expand_data)
                    data_parent = self.G.Nodes(id, :).data;
                    a = self.G.Nodes(id, :).actionset.A(aidx);
                    self.G.Nodes(id_expand, :).data = self.f_expand_data(a, data_parent);
                end
            end
        end
        
        %%
        function [self, Fs_sim, aseqs_sim] = simulate(self, id)
            %%            
            numSim = self.numSim; %#ok<*PROPLC>
            
            Fs_sim = zeros(1, numSim);
            aseqs_sim = cell(1, numSim);
            
            [nseq_hist, aseq_hist] = self.getseq(id);
            data_hist = self.G.Nodes(nseq_hist, :).data;
            
            %%
            f_rollout = self.f_rollout;
            f_reward = self.f_reward;
            node = self.G.Nodes(id, :);
            
            if self.parsim && self.numSim > 1
                parfor ii=1:numSim
                    aseq_rand = f_rollout(id, node, self);
                    fs = f_reward(aseq_hist, aseq_rand, data_hist);

                    Fs_sim(ii) = fs;
                    aseqs_sim{ii} = [aseq_hist, aseq_rand];
                end
            else
                for ii=1:numSim
                    aseq_rand = f_rollout(id, node, self);
                    fs = f_reward(aseq_hist, aseq_rand, data_hist);

                    Fs_sim(ii) = fs;
                    aseqs_sim{ii} = [aseq_hist, aseq_rand];
                end
            end
        end
        
        %%
        function self = backpropagate(self, id, Fs_sim, aseqs_sim)
            %%
            numSim = numel(Fs_sim);            
            
            %%
            [F_sim_best, idx_sim_best] = max(Fs_sim);
            aseq_sim_best = aseqs_sim{idx_sim_best};
            
            %%
            mvalue = max(Fs_sim);
            mindice = find(max(Fs_sim)==Fs_sim);
            if mvalue>self.F_best
                self.F_best = mvalue;
                self.aseq_best = aseqs_sim(mindice);
            elseif mvalue == self.F_best
                self.aseq_best = uniquec([self.aseq_best(:); aseqs_sim(mindice)']);
            end
            
            %% STORE GLOBALLY BEST ROLLOUTS
            F_best = [self.best.F, Fs_sim];
            aseq_best = [self.best.aseq, aseqs_sim];
            
            uF_best = maxk(sort(unique(F_best), 'descend'), self.numBest);
            array = F_best >= uF_best(end);
            F_best = F_best(array);
            aseq_best = aseq_best(array);
            
            [F_best, indice] = sort(F_best, 'descend');
            aseq_best = aseq_best(indice);
            
            self.best = struct();
            self.best.F = F_best;
            self.best.aseq = aseq_best;
            
            %% UPDATE MEAN ON EACH NODE
            nseq = self.getseq(id);
            
            Ns = self.G.Nodes.N(nseq);
            means = self.G.Nodes.mean(nseq);
            self.G.Nodes.mean(nseq) = (means.*Ns + sum(Fs_sim)) ./ (Ns + numSim);
            self.G.Nodes.N(nseq) = Ns + self.numSim;
            
            self.G.Nodes.ucb(nseq(2:end)) = self.getucb(nseq);
            
            %% STORE BEST ROLLOUT ON EACH NODE
            for id=nseq
                if self.G.Nodes.rollout(id).F < F_sim_best
                    self.G.Nodes.rollout(id).F = F_sim_best;
                    self.G.Nodes.rollout(id).aseq = aseq_sim_best;
                end
            end
        end
    end
            
    methods
        %%
        function set(self, prop, value)
            error('Not implemented');
            
            self.(prop) = value;
            if strcmp(prop, 'Cp')
                nodes = self.G.Nodes;
                self.G.Nodes.ucb(2:end) = self.getucb(nodes.pid(2:end), 2:size(nodes, 1));
            end
        end
        
        %%
        function [nseq, aseq] = getseq(self, id, G)
            if nargin == 2
                G = self.G;
            end
            
            %%
            [nseq, ~, indice] = G.shortestpath(1, id);
            if isempty(indice)
                aseq = [];
            else
                aseq = G.Edges.a(indice)';
            end
        end
        
        %% 
%         function ucb = getucb(self, id_parent, ids_child)
        function ucb = getucb(self, varargin)
%   ucb = SELF.GETUCB(id_parent, ids_child)
%   ucb = SELF.GETUCB(idseq)
            G = self.G;
            
            switch numel(varargin)
                case 1
                    idseq = varargin{1};
                    
                    means = G.Nodes.mean(idseq(2:end));
                    Np = G.Nodes.N(idseq(1:end-1));
                    N = G.Nodes.N(idseq(2:end));
                case 2
                    id_parent = varargin{1};
                    ids_child = varargin{2};
                    
                    means = G.Nodes.mean(ids_child);
                    Np = G.Nodes.N(id_parent);
                    N = G.Nodes.N(ids_child);
                otherwise
                    error('Invalid arguments');
            end
            
            %%
            ucb = means + 2 * self.Cp * sqrt(2*log(Np)./N);
        end
        
        %%
        function [self, id_added] = add(self, pid, aidx)            
            %%
            if pid < 0 || pid > 0 && self.G.numnodes < pid
                error('Parent leaf does not exist');
            end
            
            %%
            if pid > 0
                if self.G.Nodes.actionset(pid).arrayA(aidx) == 0
                    error('Action already taken');
                end
                
                tt_node = self.varargin2node();
                tt_edge = array2table(self.G.Nodes.actionset(pid).A(aidx), 'VariableNames', {'a'});
                [self, id_added] = add@Tree3(self, pid, tt_node, tt_edge);
            
                self.G.Nodes.actionset(pid).arrayA(aidx) = 0;
                A_added = self.f_actions(id_added, self.G.Nodes(id_added, :), self);
                
                tt_node = self.G.Nodes(id_added, :);
                tt_node.actionset.A(1, :) = A_added;
                tt_node.actionset.arrayA = ones(1, numel(A_added));
                
                self.G.Nodes(end, :) = tt_node;
            elseif pid == 0 && self.numel == 0     
                [self, id_added] = add@Tree3(self, pid);
                
                A_added = self.f_actions(1, self.G.Nodes(1, :), self);
                
                self.G.Nodes.actionset(1).A = A_added;
                self.G.Nodes.actionset(1).arrayA = ones(1, numel(A_added));
            else
                error();
            end
        end
    end
    
    
    %%
    methods
        %%
        function bool = isexpandable(self)
            bool = any(self.G.Nodes.A(:));
        end
        
        %%
        function H = plot(self)
            G = self.G;

            h = plot(G, 'layout', 'layered');
            
            h.NodeColor = 'none';
            h.EdgeColor = ones(1, 3)*0.6;
            
            if size(G.Nodes, 1) < 100
                h.NodeLabel = arrayfun(@(n)num2str(n), unique(G.Edges.EndNodes(:))', 'uni', 0);
                h.EdgeLabel = arrayfun(@(n)num2str(n), G.Edges.a, 'Uni', false);
            end            
            
            hold on;
            [~, sindice] = sort(G.Nodes.mean');
            h2 = scatter(h.XData(sindice), h.YData(sindice), 40, G.Nodes.mean(sindice)', 'filled', 'MarkerFaceAlpha', 0.8);
%             h2 = scatter(h.XData, h.YData, 40, G.Nodes.mean', 'filled', 'MarkerFaceAlpha', 0.8);
%             hold off;
            colormap('jet');
            caxis([min(G.Nodes.mean), max(G.Nodes.mean)]);
%             caxis([min(0, min(G.Nodes.mean)), max(1, max(G.Nodes.mean))]);
            colorbar;
            
            h.Parent.YTick = 1:max(G.Nodes.depth)+1;
            h.Parent.YTickLabel = (max(G.Nodes.depth):-1:0)';
            h.Parent.YGrid = 'on';
            
            H = [h, h2];
        end
    end
end