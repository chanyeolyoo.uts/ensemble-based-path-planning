
% function [X, Y] = latlon2xy(Lat, Lon, refLat, refLon)
function [X, Y] = latlon2xy(varargin)

    switch nargin
        case 3
            Lat = varargin{1};
            Lon = varargin{2};
            refLat = varargin{3}(1);
            refLon = varargin{3}(2);
        case 4
            Lat = varargin{1};
            Lon = varargin{2};
            refLat = varargin{3};
            refLon = varargin{4};
        otherwise
            error('invalid arguments');
    end

    sizeMat = size(Lat);

    Lat = Lat(:);
    Lon = Lon(:);
    
    array = ~isinf(Lat) & ~isnan(Lat) & ~isinf(Lon) & ~isnan(Lon);

    P_array = lla2flat([Lat(array), Lon(array), zeros(sum(array), 1)], [refLat, refLon], 0, 0);
    
    %%
    X = zeros(sizeMat);
    Y = zeros(sizeMat);
    
    Y(array) = P_array(:, 1);
    X(array) = P_array(:, 2);
    
    Y(~array) = Lon(~array);
    X(~array) = Lat(~array);
end