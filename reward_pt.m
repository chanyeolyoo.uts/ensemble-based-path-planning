
function S = reward_pt(X, xg, r0, rg)

mLg = size(X, 3);

[Dg, Lg] = min(sqrt(sum((X-xg).^2, 2)), [], 3); % Dg=min dist to goal; Lg=Path length from x0 to min point

A = 0.1 * exp( r0*log(9) / (r0-rg) );
B = log(9) / (r0-rg);

S = A * exp(-B * Dg);

%%
array = Dg <= rg;
% S(Dg<=rg) = 1;
S(array) = exp(-log(10/9)/(mLg-1) * (Lg(array)-1));


% function S = reward_pt(D, r0, rg)
% 
% A = 0.1 * exp( r0*log(9) / (r0-rg) );
% B = log(9) / (r0-rg);
% 
% S = A * exp(-B * D);
% S(D<=rg) = 1;
% 
