
% function [th, thidx] = randsteer(x0, x1, Th, N)
function [th, thidx] = randsteer(th0, Th, sigma, varargin)

K = numel(Th);

% [~, midx] = min(abs(angdiff(Th, atan2(x1(2)-x0(2), x1(1)-x0(1)))));

% th = atan2(x1(2)-x0(2), x1(1)-x0(1));

%%
% D = zeros(size(Th));
% for ii=1:numel(D)
%     D(ii) = angdiff(th0, Th(ii));
% end
% pdf = normpdf(D, 0, sigma);
% pdf = pdf ./ sum(pdf);
% cdf = cumsum(pdf);

numTh0 = numel(th0);
numTh = numel(Th);

D = zeros(numTh0, numTh+1);
for ii=1:numTh0
    th0_cur = th0(ii);
    for jj=1:numTh
        D(ii, jj+1) = angdiff(th0_cur, Th(jj));
    end
end
D(:, 1) = inf;

pdf = normpdf(D, 0, sigma);
cdf = cumsum(pdf, 2);
cdf = cdf ./ cdf(:, end);

if numTh0 == 1
    R = rand(varargin{:});
    thidx = arrayfun(@(r) find(r<=cdf(2:end) & r>cdf(1:end-1)), R);
    th = Th(thidx);
else
    R = rand(numTh0, 1);
    mat = R<=cdf(:, 2:end) & R>cdf(:, 1:end-1);
    th = zeros(size(th0));
    for ii=1:numTh0
        th(ii) = Th(mat(ii, :));
    end
end