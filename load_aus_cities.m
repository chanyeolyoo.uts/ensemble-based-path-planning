
function tt = load_aus_cities()

c = {'Sydney'	-33.870	151.210;
'Melbourne'	-37.810	144.960;
'Brisbane'	-27.460	153.020;
'Perth'	-31.960	115.840;
'Adelaide'	-34.930	138.600;
'Newcastle'	-32.920	151.750;
'Gold coast'	-28.070	153.440;
'Canberra'	-35.310	149.130;
'Wollongong'	-34.420	150.870;
% 'Sunshine coast'	-25.880	152.560;
'Hobart'	-42.850	147.290;
% 'Geelong'	-38.140	144.320;
'Townsville'	-19.260	146.780;
'Cairns'	-16.920	145.750;
'Launceston'	-41.450	147.130;
% 'Albury-wodonga'	-36.060	146.920;
'Darwin'	-12.430	130.850;
% 'Toowoomba'	-27.560	151.960;
% 'Ballarat'	-37.560	143.840;
% 'Shoalhaven'	-34.880	150.590
'Jervis Bay'    -35.122038, 150.707543;
'Batemans Bay'  -35.708056, 150.174444;
};

tt = cell2table(c, 'VariableNames', {'name', 'lat', 'lon'});

% plot(tt.lon, tt.lat, 'r*');
% set(text(tt.lon, tt.lat, tt.name), 'horizontalAlignment', 'right');

end
