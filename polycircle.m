
function varargout = polycircle(x, y, r, varargin)

% POLYCIRCLE generates polygon circle
%   ... = POLYCIRCLE() Gives unit circle at (0, 0) with radius 1 and N = 10.
%   ... = POLYCIRCLE(N) Gives unit circle at (0, 0) with radius 1
%   poly = POLYCIRCLE(x, y, r)
%   poly = POLYCIRCLE(x, y, r)
%   [polyX, polyY] = POLYCIRCLE(x, y, r)
%   [polyX, polyY] = POLYCIRCLE(x, y, r, 'N', N)

%% PARSING
parser = inputParser;
parser.addParameter('N', 10);
parser.addParameter('Theta', [0, 2*pi]);

parser.parse(varargin{:});

%% ARGUMENTS
if nargin == 0
    x = 0;
    y = 0;
    r = 1;
    N = 10;
elseif nargin == 1
    N = x;
    x = 0;
    y = 0;
    r = 1;
else
    N = parser.Results.N;
end

%%
Theta = parser.Results.Theta;
if Theta(2) < Theta(1)
    Theta(2) = Theta(2) + 2*pi;
end

%%
% th = linspace(0, 2*pi, N-1);
% th = linspace(Theta(1), Theta(2), N-1);
th = linspace(Theta(2), Theta(1), N+1);
th = th(1:end-1);
polyX = x + r*cos(th);
polyY = y + r*sin(th);

polyX(end+1) = polyX(1);
polyY(end+1) = polyY(1);

% [polyX, polyY] = poly2cw(polyX, polyY);

if nargout == 1
    varargout{1} = [polyX(:), polyY(:)];
elseif nargout == 2
    varargout{1} = polyX;
    varargout{2} = polyY;
else
    error('Incorrect number of outputs');
end
