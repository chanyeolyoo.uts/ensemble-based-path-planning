
function array = vertcatc(carray)

%VERTCATC Cell array to array using vertcat
%   array = VERTCATC(carray)
%
%Written by Chanyeol Yoo, Ph.D., University of Technology Sydney, 2019.



array = vertcat(carray{:});