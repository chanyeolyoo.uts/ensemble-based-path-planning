
classdef Tree3
    
    %%
    properties
        G
    end
    
    properties (SetAccess = private)
        node_default;
        edge_default;
    end
    
    %%
    methods
        %%
        function self = Tree3(varargin)
%Constructor for Tree class
%   Tree(st) for nodes with no edges
%   Tree('name1', data1, 'name2', data2, ...) for nodes with no edges
%
%   Tree(st_node, st_edge) for node and edge
%   Tree({...}, {...}) where first cell is for node and the second is for edges

            %% INITIALISE DIGRAPH
            self.G = digraph;
            
            %%
            if nargin == 1 && istable(varargin{1})  % NODE ONLY WITH TABLE
                tt_node = struct2table(varargin{1});
                tt_edge = table();
            elseif nargin == 2 && istable(varargin{1}) && istable(varargin{2})  % NODE AND EDGE WITH TABLE
                tt_node = struct2table(varargin{1});
                tt_edge = struct2table(varargin{2});
            elseif nargin == 2 && iscell(varargin{1}) && iscell(varargin{2})    % NODE AND EDGE WITH VARARGIN
                tt_node = cell2table(varargin{1}(2:2:end), 'VariableNames', varargin{1}(1:2:end));
                tt_edge = cell2table(varargin{2}(2:2:end), 'VariableNames', varargin{2}(1:2:end));
            elseif nargin > 1 && all(cellfun(@(c) ischar(c), varargin(1:2:end)))    % NODE ONLY WITH VARARGIN
                tt_node = cell2table(varargin(2:2:end), 'VariableNames', varargin(1:2:end));
                tt_edge = table();
            else
                error('Invalid leaf setting');
            end
            tt_node = [table(nan, nan, 'VariableNames', {'pid', 'depth'}), tt_node];
            
            %%            
            self.node_default = tt_node;
            self.edge_default = tt_edge;
        end
        
        %%
        function [self, node_id] = add(self, parent, varargin)
%Adding a leaf to tree
%   Tree.add(parent, st) adds struct variable st to parent 
%   Tree.add(parent, 'name1', data1, 'name2', data2, ...) adds struct in 
%   varargin. The struct should be the same as 'tt_suffix' pre-defined in constructor
%
%   Tree.add(parent, st_node, st_edge)
%   Tree.add(parent, {'name1', var1, ...}, {'name1', var1, ...})
            %%
            G = self.G;
            
            %%
            node_id = self.G.numnodes + 1;
            
            if parent ~= 0 && G.numnodes < parent
                error('Parent leaf does not exist');
            end

            %%
            if numel(varargin) == 0
                tt_node = self.varargin2node([]);
                tt_edge = self.varargin2edge([]);
            elseif numel(varargin) == 1  % NODE ONLY WITH TABLE
                tt_node = self.varargin2node(varargin{:});
                tt_edge = self.varargin2edge([]);
            elseif numel(varargin) == 2 && istable(varargin{1}) && istable(varargin{2})
                tt_node = varargin{1};
                tt_edge = varargin{2};
            elseif numel(varargin) == 2 && iscell(varargin{1}) && iscell(varargin{2})
                tt_node = self.varargin2node(varargin{1}{:});
                tt_edge = self.varargin2edge(varargin{2}{:});
            elseif numel(varargin) > 1 && all(cellfun(@(c) ischar(c), varargin(1:2:end)))    % NODE ONLY WITH VARARGIN
                tt_node = self.varargin2node(varargin{:});
                tt_edge = self.varargin2edge([]);
            else
                error('Invalid leaf setting');
            end
            
            %%
            tt_node.pid = parent;
            if parent == 0
                tt_node.depth = 0;
            else
                tt_node.depth = G.Nodes.depth(parent)+1;
            end
            
            G = G.addnode(tt_node);
            if parent > 0
                if isempty(tt_edge)
                    G = G.addedge(parent, node_id);
                else
                    G = G.addedge(parent, node_id, tt_edge);
                end
            end
            
            %%
            self.G = G;
        end
        
        %%
        function self = set(self, id, varargin)
            G = self.G;
            for ii=1:2:numel(varargin)
                G.Nodes{id, varargin{ii}} = varargin{ii+1};
            end
            self.G = G;
        end
        
        %%
        function obj = get(self, varargin)
%Get object from tree
%   obj = Tree.get(att)
%   obj = Tree.get(node_id)
%   obj = Tree.get(node_id, att)

            Nodes = self.G.Nodes;

            if nargin == 2
                if isnumeric(varargin{1})   % e.g., obj = Tree.get(node_id)
                    obj = Nodes(varargin{1}, :);
                elseif ischar(varargin{1})  % e.g., obj = Tree.get(att)
                    obj = Nodes.(varargin{1});
                else
                    error('Invalid input arguments');
                end
            elseif nargin == 3  % e.g., obj = Tree.get(node_id, att)
                obj = Nodes{varargin{1}, varargin{2}};
            else
                error('Invalid input arguments');
            end
        end
        
        %%
        function obj = getchild(self, parent, varname)
            if ~exist('varname', 'var')
                obj = self.G.Nodes(self.G.Edges.EndNodes(:, 1)==parent, :);
            else
                obj = self.G.Nodes{self.G.Edges.EndNodes(:, 1)==parent, varname};
            end
        end
        
        %%
        function [nseq, eseq] = getseq(self, id)
            %%
            [nseq, ~, indice] = self.G.shortestpath(1, id);
            if isempty(indice)
                eseq = [];
            else
                eseq = self.G.Edges(indice, :);
            end
        end
        
        %%
        function nodes = Nodes(self, varargin)
            if numel(varargin) == 0
                nodes = self.G.Nodes;
            elseif numel(varargin) == 1
                nodes = self.G.Nodes(varargin{1}, :);
            else
                nodes = self.G.Nodes{varargin{:}};
            end
        end
        
        %%
        function nodes = Edges(self, varargin)
            if numel(varargin) == 0
                nodes = self.G.Edges;
            elseif numel(varargin) == 1
                nodes = self.G.Edges(varargin{1}, :);
            else
                nodes = self.G.Edges{varargin{:}};
            end
        end
        
        %%
        function num = numel(self)
            num = self.G.numnodes;
        end
    end
    
    %%
    methods
        %%
        function tt_row = varargin2node(self, varargin)
            tt_row = varargin2row(self, self.node_default, varargin{:});
        end
        
        %%
        function tt_row = varargin2edge(self, varargin)
            tt_row = varargin2row(self, self.edge_default, varargin{:});
        end
        
        %%
        function tt_row = varargin2row(self, tt_base, varargin)
            tt_row = tt_base;
            varnames = tt_base.Properties.VariableNames;
            
            if numel(varargin) == 1 && ~isempty(varargin{:})
                % Struct input
                st = varargin{1};
                names = fieldnames(st);
                if ~all(ismember(names, varnames))
                    error('Invalid variable name');
                end
                
                for ii=1:numel(names)
                    tt_row{1, names{ii}} = st.(names{ii});
                end
            elseif numel(varargin) > 1
                % varargin input
                if ~all(ismember(varargin(1:2:end), varnames))
                    error('Invalid variable name');
                end
                
                st = cell2table(varargin(2:2:end), 'VariableNames', varargin(1:2:end));
                for ii=1:2:numel(varargin)-1
%                     tt_row{1, varargin{ii}} = st.(varargin{ii});
                    tt_row.(varargin{ii}) = st.(varargin{ii});
                end                
            end
        end
    end
end