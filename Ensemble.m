
classdef Ensemble
    %%
    properties
        Xe = [];
        Ye = [];
        
        Ue = [];
        Ve = [];
        
        Xe_gpu = [];
        Ye_gpu = [];
        
        numP = 0;   % number of points
        numE = 0;   % number of ensemble forecast
    end
    
    properties
        usegpu = true;
    end
    
    %%
    methods
        %%
        function obj = Ensemble(arg_Xe, arg_Ye, arg_Ue, arg_Ve, varargin)
            p = inputParser;
            p.addParameter('usegpu', true);
            p.parse(varargin{:});
            
            obj.usegpu = p.Results.usegpu & gpuDeviceCount > 0;
            if p.Results.usegpu==true && gpuDeviceCount == 0
                warning('No GPU found');
            end
            
            obj.Xe = arg_Xe;
            obj.Ye = arg_Ye;
            obj.Ue = arg_Ue;
            obj.Ve = arg_Ve;
            

                        
            [obj.numP, obj.numE] = size(obj.Ue);
            
            if obj.usegpu
                obj.Xe_gpu = gpuArray(arg_Xe);
                obj.Ye_gpu = gpuArray(arg_Ye);
            end
            
        end
        
        %%
        function obj = Trim(obj, sizeMap)
            array = obj.Xe>=sizeMap(1) & obj.Xe<=sizeMap(2) & obj.Ye>=sizeMap(3) & obj.Ye<=sizeMap(4);
            obj.Xe = obj.Xe(array);
            obj.Ye = obj.Ye(array);
            obj.Ue = obj.Ue(array, :);
            obj.Ve = obj.Ve(array, :);
            
            if obj.usegpu
                obj.Xe_gpu = obj.Xe_gpu(array, :);
                obj.Ye_gpu = obj.Ye_gpu(array, :);
            end
            
            obj.numP = sum(array);
        end
        
        %%
        function obj = TrimAvg(obj)
            obj.Ue = mean(obj.Ue, 2);
            obj.Ve = mean(obj.Ve, 2);
            obj.numE = 1;
        end
        
        %%
        function [ind, dind] = QueryIndex(obj, xq, yq)
            dq = size(xq);
            
            if ~all(size(xq)==size(yq))
                error('Input size does not match');
            end
            
            
            if obj.usegpu
                [dind, ind] = min((xq(:)'-obj.Xe_gpu).^2 + (yq(:)'-obj.Ye_gpu).^2, [], 1);
            else
                [dind, ind] = min((xq(:)'-obj.Xe).^2 + (yq(:)'-obj.Ye).^2, [], 1);
            end
            ind = reshape(ind, dq);
            dind = reshape(dind, dq);
        end
        
        %%
        function [ueq, veq] = QueryAll(obj, xq, yq)
            mindice = obj.QueryIndex(xq, yq);
            ueq = obj.Ue(mindice(:), :);
            veq = obj.Ve(mindice(:), :);
        end
        
        %%
        function [uq, vq] = Query(obj, Xeq, Yeq, eidx)
%             if numel(varargin) == 1
%                 Xeq = varargin{1}(:, 1);
%                 Yeq = varargin{1}(:, 2);
%             else
%                 Xeq = varargin{1};
%                 Yeq = varargin{2};
%             end
%             
%             mindice = obj.QueryIndex(Xeq, Yeq);
%             if numel(varargin) == 3
%                 indice = mindice(:) + obj.numP * (eidx-1);
%             else
%                 indice = mindice(:) + obj.numP * (0:obj.numE-1)';
%             end
            mindice = obj.QueryIndex(Xeq, Yeq);
            if nargin == 4
                indice = mindice(:) + obj.numP * (eidx-1);
            else
                indice = mindice(:) + obj.numP * (0:obj.numE-1)';
            end
            
            uq = obj.Ue(indice);
            vq = obj.Ve(indice);
        end
    end
end