
function h = getfigure(ifig, opt)

% GETFIGURE Get figure focus when not focused. Create when no exist
%   h = GETFIGURE(ifig): h = figure handle; ifig = figure number

cfig = get(get(0, 'CurrentFigure'), 'Number');
if isempty(cfig)
    cfig = 0;
end

if cfig ~= ifig
    h = figure(ifig);
else
    figs = get(0, 'Children');
    h = figs([figs.Number]==ifig);
end

if nargin == 2
    if opt == true
        clf; hold on; grid on; setFigTight;
    end
end