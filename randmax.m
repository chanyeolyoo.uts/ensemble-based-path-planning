
function [mvalue, midx] = randmax(M)

% RANDMAX Find the maximum value and index. If there are more than one
% maximum value, then randomly choose one
%   [mvalue, midx] = RANDMAX(M)
%
% Written by Chanyeol Yoo, University of Technology Sydney, 2018

mvalue = max(M);
indice = find(M==mvalue);
midx = indice(randi(numel(indice)));
