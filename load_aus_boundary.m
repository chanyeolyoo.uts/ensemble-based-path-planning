
function varargout = load_aus_boundary()

% LOAD_AUS_BOUNARY Returns Australian boundary in lat/lon
% [LAT, LON] = LOAD_AUS_BOUNDARY()
% P = LOAD_AUS_BOUNDARY(), where P = [LAT(:), LON(:)]
%
% Written by Chanyeol Yoo, University of Technology Sydney, 2018


    stack = dbstack;
%     filename = stack(2).file;

%     S = shaperead('../Resources/AUS_adm_shp/AUS_adm0.shp');
%     S = shaperead(fullfile(fileparts(which(stack(1).name)), 'Resources/AUS_adm_shp/AUS_adm0.shp'));
%     S = shaperead(fullfile(fileparts(which(stack(1).name)), '/AUS_adm_shp/AUS_adm0.shp'));
%     S = shaperead(fullfile(fileparts(which(stack(1).name)), '../resources/AUS_adm_shp/AUS_adm0.shp'));
    S = shaperead(fullfile(fileparts(which(stack(1).name)), '../resources/AUS_adm_shp/AUS_adm0.shp'));
%     S = shaperead('./AUS_adm_shp/AUS_adm0.shp');
    
    if nargout == 1
        varargout{1} = struct('lat', S.Y, 'lon', S.X);
    elseif nargout == 2
        varargout{1} = [S.Y];
        varargout{2} = [S.X];
    else
        error('Invalid number of outputs');
    end
end
