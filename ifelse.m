
function r = ifelse(condition, rIfTrue, rIfFalse)

    if condition
        if isa(rIfTrue, 'function_handle')
            r = rIfTrue();
        else
            r = rIfTrue;
        end
    else
        if isa(rIfFalse, 'function_handle')
            r = rIfFalse();
        else
            r = rIfFalse;
        end
    end

end