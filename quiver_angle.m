
function varargout = quiver_angle(X, Y, Theta, varargin)
%QUIVER_ANGLE Quiver function using angles not vectors
%   h = QUIVER_ANGLE(X, Y, Theta) is equivalent to 
%   QUIVER(X,Y,cos(Theta),sin(Theta)).
%   h = QUIVER_ANGLE(X, Y, Theta, ...) where ... corresponds to additional 
%   arguments in QUIVER function.

    h = quiver(X, Y, cos(Theta), sin(Theta), varargin{:});
    if nargout == 1
        varargout{1} = h;
    end
end