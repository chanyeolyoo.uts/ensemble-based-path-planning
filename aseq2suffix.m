
function nid = aseq2suffix(aseq, mcts)


EndNodes = mcts.G.Edges.EndNodes;
A = mcts.G.Edges.a;

nid = 1;
% nseq = nid;
for aidx=1:numel(aseq)
    nindice = find(EndNodes(:, 1)==nid);
    nid_cur = EndNodes(nindice(A(nindice)==aseq(aidx)), 2);
    if isempty(nid_cur)
        break;
    end
    nid = nid_cur;
%     nseq = [nseq, nid];
end
