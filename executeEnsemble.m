
function [X, Xc] = executeEnsemble(x0, seq, ens, dt, T, v0)

numSeq = numel(seq);
numStepPerAct = ceil(T/dt);

switch size(x0, 1)
    case 1
        X_cur = repmat(x0, ens.numE, 1);
    case ens.numE
        X_cur = x0;
    otherwise
        error('Incorrect number for x0');
end
Xc = cell(1, numSeq);

%%
vm = v0+max(sqrt((ens.Ue(:)).^2+(ens.Ve(:)).^2));
dm = vm * T * numel(seq);
ens = ens.Trim([minmax(x0(:, 1)') + [-1, 1]*dm, minmax(x0(:, 2)') + [-1, 1]*dm]);

%%
for seqidx=1:numSeq
%     th = seq(seqidx);
%     vel = 0.35 * [cos(th), sin(th)];
    a = seq(seqidx);
    
    X = nan([size(X_cur), numStepPerAct]);
    X(:, :, 1) = X_cur;
    
    
    [ue, ve] = ens.Query(X_cur(:, 1), X_cur(:, 2)); % APPROXIMATING THAT NO CURRENT CHANGE DURING AN ACTION
    for stepidx=2:numStepPerAct
%         [ue, ve] = ens.Query(X_cur(:, 1), X_cur(:, 2));
%         X_cur = X_cur + dt * (vel + [ue, ve]);
        X_cur = X_cur + dt * ((v0 * [cos(a), sin(a)]) + [ue, ve]);
        X(:, :, stepidx) = X_cur;
    end
    Xc{seqidx} = X;
end

X = Xc{1};
for ii=2:numel(Xc)
    X = cat(3, X, Xc{ii}(:, :, 2:end));
end