
function ang = atan2m(x0, x1)

ang = atan2(x1(2)-x0(2), x1(1)-x0(1));