
function y = rotspace(N)
%ROTSPACE linearly spaced vector for a rotational space
%   ROTSPACE(N) generates N points between [0, 2*pi)
%
%Written by Chanyeol Yoo, Ph.D., University of Technology Sydney, 2018.

y = linspace(0, 2*pi, N+1);
y(end) = [];