
function cc = uniquec(C)

mat = zeros(numel(C));
for ii=1:numel(C)
    for jj=(ii+1):numel(C)
        mat(ii, jj) = isequal(C{ii}, C{jj});
    end
end

cc = C;
cc(any(mat)) = [];
