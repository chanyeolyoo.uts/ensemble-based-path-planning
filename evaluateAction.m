
function st = evaluateAction(a, data_parent, param)

x0 = data_parent.X(:, :, end);
status = data_parent.status;

X = executeEnsemble(x0, a, param.ens, param.dt, param.T, param.v0);

array_g = any(inpolygon(X(:, 1, :), X(:, 2, :), param.x_goal(1), param.x_goal(2)), 3);

status(array_g) = 1;

st = struct('status', status, 'X', X);