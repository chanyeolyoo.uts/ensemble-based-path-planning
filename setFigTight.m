
function setFigTight(axes_handle)

if nargin == 0
    axes_handle = gca;
end

set(axes_handle, 'LooseInset', get(axes_handle, 'TightInset'));
% set(axes_handle, 'LooseInset', get(axes_handle, 'TightInset') .* [1, 1.2, 1, 1])