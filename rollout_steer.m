
function [aseq_rand, score_rollout] = rollout_steer(node, param, sigma)

eidx = randi(param.ens.numE);

x = node.data.X(eidx, :, end);

th0 = atan2m(x, param.x_goal);

[uq, vq] = param.ens.Query(x(1), x(2), eidx);

vec = param.v0 * [cos(param.Th)', sin(param.Th)'] + [uq, vq];

[~, midx] = min(abs(arrayfun(@(th1) angdiff(th0, th1), atan2(vec(:, 2), vec(:, 1)))));
th = param.Th(midx);

% aseq_rand = randsteer(th, param.Th, param.hrz - node.depth);
aseq_rand = randsteer(th, param.Th, sigma, [1, param.hrz - node.depth]);